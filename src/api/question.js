
import { collection, doc, documentId, FieldPath, getDocs, limit, query, setDoc, where } from "firebase/firestore";
import { getDownloadURL, ref, uploadBytes } from "firebase/storage";
import { v4 as uuidv4 } from 'uuid';
import { db, storage } from "../firebase/firebase";

export const saveImageFile = async (imgFile, id) => {
    var re = /(?:\.([^.]+))?$/;
    const ext = re.exec(imgFile.name)[1]
    const filename = id + "." + ext
    const storageRef = ref(storage, `question-images/${filename}`);
    await uploadBytes(storageRef, imgFile);
    return storageRef
}

export const getBonusQuestion = async (qid) => {
    const q = query(collection(db, 'questions'),
        where("bonusParent", '==', qid),
        limit(1000)
    )
    const snap = await getDocs(q)
    const questions = []
    snap.forEach((doc) => {
        questions.push({
            ...doc.data(),
            id: doc.id
        })
    })
    return questions
}

export const addQuestion = async (questionData) => {
    const id = uuidv4()
    await saveQuestion(questionData, id);
}

export const editQuestion = async (questionData, id) => {
    await saveQuestion(questionData, id);
}

export const getQuestions = async (tags) => {
    var q = null
    if (tags && tags.length > 0) {
        q = query(collection(db, 'questions'),
            where("tags", 'array-contains-any', tags),
            limit(1000)
        )
    } else {
        return []
    }
    const snap = await getDocs(q)
    const questions = []
    const bonusParentSet = []
    snap.forEach((doc) => {
        if (doc.data().bonusParent) {
            bonusParentSet.push(doc.data().bonusParent)
        }
    })
    snap.forEach((doc) => {
        if (!doc.data().bonusParent) {
            questions.push({
                ...doc.data(),
                id: doc.id,
                bonusPresent: bonusParentSet.includes(doc.id)
            })
        }
    })
    return questions
}

export const getQuestionsForKeyWords = async (kwd) => {
    if (kwd && kwd.length > 0) {
        const qKwd = query(collection(db, 'keywords'),
            where("keywords", 'array-contains-any', kwd)
        )
        const snapKwd = await getDocs(qKwd)
        const qids = []
        snapKwd.forEach((doc) => {
            qids.push(doc.data().qid)
        })

        const questions = []
        if (qids && qids.length > 0) {
            const chunkSize = 10;
            for (let i = 0; i < qids.length; i += chunkSize) {
                const chunk = qids.slice(i, i + chunkSize);

                const qQ = query(collection(db, 'questions'),
                    where(documentId(), 'in', chunk)
                )
                const snap = await getDocs(qQ)

                const bonusParentSet = new Set < String > ([])
                snap.forEach((doc) => {
                    if (doc.data().bonusParent) {
                        bonusParentSet.add(doc.data().bonusParent)
                    }
                })
                snap.forEach((doc) => {
                    if (!doc.data().bonusParent) {
                        questions.push({
                            ...doc.data(),
                            id: doc.id,
                            bonusPresent: bonusParentSet.has(bonusParentSet)
                        })
                    }

                })
            }
            return questions
        } else {

            return []
        }

    } else {
        return []
    }
}

async function saveQuestion(questionData, id) {
    const { imgFile, ...data } = questionData;

    if (questionData.imgFile) {

        const storageRef = await saveImageFile(questionData.imgFile, id);
        const downLoadURL = await getDownloadURL(storageRef)
        data.image = downLoadURL
    }

    const docRef = doc(db, "questions", id);
    await setDoc(docRef, data, { merge: true });
}
