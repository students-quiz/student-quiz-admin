
import { collection, getDocs, query, where } from "firebase/firestore";
import { db } from "../firebase/firebase";


export const getLeafTags = async () => {

    const q = query(collection(db, 'tags'),
        where("leaf", "==", true),
    )

    const querySnapshot = await getDocs(q);
    const tags = []
    querySnapshot.forEach( doc => tags.push(doc.data().display_name))
    return tags     
}