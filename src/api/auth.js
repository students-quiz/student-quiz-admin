import cryptoJs from "crypto-js";
import { browserLocalPersistence, createUserWithEmailAndPassword, setPersistence, signInWithEmailAndPassword } from "firebase/auth";
import { doc, getDoc } from 'firebase/firestore';
import { db, fireAuth } from "../firebase/firebase";
export const auth_encrypt_key = 'AuthSettings';

export async function register({ email, password }) {
    try {

        const auth = fireAuth;
        const { user } = await createUserWithEmailAndPassword(auth, email, password);
        return user;
    } catch (error) {
        return Promise.reject(error.message);
    }
}

export const fetchUser = async (userid) => {
    const docRef = doc(db, "users", userid);
    const docSnap = await getDoc(docRef);
    const data = docSnap.data()
    console.log("Document data:", docSnap.data());
    return data
}

export const login = async ({ email, password }) => {
    const userProfile = await firebaseLogin(email, password);
    saveAuthSettings(email, password);
    return userProfile;
}

export const saveAuthSettings = (email, password) => {
    const currentSettings = localStorage.getItem('auth-settings');
    const parsedCurrentSettings = currentSettings ? JSON.parse(currentSettings) : {};
    password = cryptoJs.AES.encrypt(password, auth_encrypt_key).toString()
    const settings = { ...parsedCurrentSettings, email, password };
    const stringifiedSettings = JSON.stringify(settings);
    localStorage.setItem('auth-settings', stringifiedSettings);
}

const getAuthSettings = () => {
    const currentSettings = localStorage.getItem('auth-settings');
    const parsedCurrentSettings = currentSettings ? JSON.parse(currentSettings) : {};
    return parsedCurrentSettings;
}

export const autoLogin = async () => {
    const auth = getAuthSettings();
    if (auth.email && auth.password) {
        const email = auth.email;
        const password = cryptoJs.AES.decrypt(auth.password, auth_encrypt_key).toString(cryptoJs.enc.Utf8)
        const userProfile = await firebaseLogin(email, password);
        console.log('Auto login ' + userProfile.email)
        return userProfile;
    }
}
async function firebaseLogin(email, password) {
    await setPersistence(fireAuth, browserLocalPersistence);
    const { user } = await signInWithEmailAndPassword(fireAuth, email, password);
    const userProfile = await fetchUser(user.uid);
    return userProfile;
}

