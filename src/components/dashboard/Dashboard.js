import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Toolbar from '@mui/material/Toolbar';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { contextActions } from '../../reducers/context-slice';
import Copyright from '../shared/Copyright';
import Chart from './Chart';
import Deposits from './Deposits';
import Orders from './Orders';

export function Dashboard() {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(contextActions.contextChanged('Dashboard'))
  }, [])
  
  return (<Box component="main" sx={{
    backgroundColor: theme => theme.palette.mode === 'light' ? theme.palette.grey[100] : theme.palette.grey[900],
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto'
  }}>
    <Toolbar />
    <Container maxWidth="lg" sx={{
      mt: 4,
      mb: 4
    }}>
      <Grid container spacing={3}>
        {
          /* Chart */
        }
        <Grid item xs={12} md={8} lg={9}>
          <Paper sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240
          }}>
            <Chart />
          </Paper>
        </Grid>
        {
          /* Recent Deposits */
        }
        <Grid item xs={12} md={4} lg={3}>
          <Paper sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column',
            height: 240
          }}>
            <Deposits />
          </Paper>
        </Grid>
        {
          /* Recent Orders */
        }
        <Grid item xs={12}>
          <Paper sx={{
            p: 2,
            display: 'flex',
            flexDirection: 'column'
          }}>
            <Orders />
          </Paper>
        </Grid>
      </Grid>
      <Copyright sx={{
        pt: 4
      }} />
    </Container>
  </Box>);
}
