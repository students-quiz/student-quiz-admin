import EditIcon from '@mui/icons-material/Edit';
import {
    Box, Button, CssBaseline, Grid, List, ListItem,
    ListItemButton, ListItemIcon, ListItemText, TextField
} from '@mui/material';
import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from "react-router-dom";
import { addQuestion, getBonusQuestion, getQuestions, getQuestionsForKeyWords } from '../../api/question';
import { getLeafTags } from '../../api/tags';
import { contextActions } from '../../reducers/context-slice';
import { convertFromHTML, convertToHTML } from 'draft-convert';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import MUIRichTextEditor from 'mui-rte';

const EDIT = "EDIT";
const ADD = "ADD";
export default function BonusQuestionsList() {
    const location = useLocation()
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const parentQData = location.state.data
    const initialDidYouKnowState = parentQData && parentQData.didyouknow ?
        convertFromHTML(parentQData.didyouknow)
        : EditorState.createEmpty().getCurrentContent()


    const [questions, setQuestions] = useState([])



    useEffect(() => {
        dispatch(contextActions.contextChanged('BonusQuestions'))
        getBonusQuestion(parentQData.id)
            .then((qs) => setQuestions(qs))
    }, [])



    return (
        <Fragment>
            <CssBaseline />
            <Grid
                container
                spacing={2}
                justifyContent="center"
                alignItems="center">
                <Grid item xs={12} minHeight={200}>
                    <Box
                        sx={{
                            marginTop: 8,
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'center',
                        }}
                    >
                        <MUIRichTextEditor
                            label="Did you know..."
                            defaultValue={JSON.stringify(convertToRaw(initialDidYouKnowState))}
                        />

                    </Box>
                </Grid>
                <Grid item xs={8} >
                    <Box
                        sx={{
                            marginTop: 2,
                            marginLeft: 5,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'start',

                        }}
                    >

                        <List sx={{ width: '100%', maxWidth: 800, bgcolor: 'background.paper' }}>
                            {questions.map((q) =>
                                <ListItem alignItems="flex-start" key={q.id}>
                                    <ListItemText
                                        primary={q.question}
                                        secondary={q.options[q.correct_option]}
                                        sx={{ width: '100%' }}

                                    />
                                    <ListItemButton onClick={() => navigate('../question', { state: { data: { ...q } } })} >
                                        <ListItemIcon>
                                            <EditIcon />
                                        </ListItemIcon>
                                    </ListItemButton>
                                </ListItem>
                            )}
                        </List>
                    </Box>
                </Grid>
                <Grid item xs={8} >
                    <Button
                        onClick={() => navigate('../addquestion', { state: { bonusParent: parentQData } })}
                        fullWidth
                        variant="contained"
                    >
                        ADD MORE QUESTION
                    </Button>
                </Grid>
            </Grid>



        </Fragment>


    )
}
