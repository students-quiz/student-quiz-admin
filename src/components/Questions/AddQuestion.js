import { createTheme, styled } from '@mui/material/styles';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { addQuestion } from '../../api/question';
import { contextActions } from '../../reducers/context-slice';
import { Question } from './Question';

const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
});

const theme = createTheme();

export default function AddQuestion() {

  const location = useLocation()
  
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const bonusParent = location.state && location.state.bonusParent ?  location.state.bonusParent : null
  useEffect(() => {
    dispatch(contextActions.contextChanged('Add  Question'))
  }, [])

  const data = bonusParent? { tags: bonusParent.tags, credits: bonusParent.credits} : null

  const submitAction = async (questionData) => {
    questionData.bonusParent = bonusParent? bonusParent.id : ''
    addQuestion(questionData).then(() => {
      console.log("Saved question")
      navigate(-1)
    }).catch(error => {
      console.log(error)
    });
  }


  return (
    <Question
      data={data}
      submitAction={submitAction}
    />
  );
}
