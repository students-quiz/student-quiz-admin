import EditIcon from '@mui/icons-material/Edit';
import {
    Box, Button, CssBaseline, Grid, List, ListItem,
    ListItemButton, ListItemIcon, ListItemText, TextField
} from '@mui/material';
import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from "react-router-dom";
import { getQuestions, getQuestionsForKeyWords } from '../../api/question';
import { getLeafTags } from '../../api/tags';
import { contextActions } from '../../reducers/context-slice';
import TagsInput from '../shared/TagsInput';
import AutoAwesomeMotionIcon from '@mui/icons-material/AutoAwesomeMotion';
import { useTheme } from '@mui/material/styles';

export default function QuestionsList() {
    const theme = useTheme();
    const [questions, setQuestions] = useState([])
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [tagsOption, setTagsOption] = useState([])



    useEffect(() => {
        dispatch(contextActions.contextChanged('Questions'))
    }, [])


    useEffect(() => {
        getLeafTags().then((tags) => setTagsOption(tags))
    }, [])

    const onTagChange = (event, selTags) => {
        console.log(selTags);
        getQuestions(selTags)
            .then((questions) => setQuestions(questions))
    }
    const onKeyWordChange = (e) => {
        if (e.keyCode == 13) {
            const kText = e.target.value
            const kwds = kText.toLowerCase().split(" ")
            getQuestionsForKeyWords(kwds)
                .then((questions) => setQuestions(questions))
        }
    }

    return (
        <Fragment>
            <CssBaseline />
            <Grid
                container
                spacing={2}
                justifyContent="center"
                alignItems="center">
                <Grid item xs={8} >
                    <Button
                        onClick={() => navigate('../addquestion')}
                        fullWidth
                        variant="contained"
                        sx={{ mt: 8, mb: 2 }}
                    >
                        ADD MORE QUESTION
                    </Button>
                </Grid>
                <Grid item xs={8}>
                    <TextField
                        fullWidth
                        onKeyDown={(e) => onKeyWordChange(e)}
                        label="Keywords"
                    />
                </Grid>
                <Grid item xs={8}>
                    <TagsInput
                        options={tagsOption}
                        onChange={onTagChange}
                        inputTags={[]}
                    />
                </Grid>
                <Grid item xs={8} >
                    <Box
                        sx={{
                            width: 500,
                            height: 300,
                            marginTop: 2,
                            marginLeft: 5,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'start',

                        }}
                    >

                        <List sx={{ width: '100%', maxWidth: 800, bgcolor: 'background.paper' }}>
                            {questions.map((q) =>
                                <ListItem alignItems="flex-start" key={q.id}>
                                    <ListItemText
                                        primary={q.question}
                                        secondary={q.options[q.correct_option]}
                                        sx={{ width: '100%' }}

                                    />
                                    <ListItemButton onClick={() => navigate('../question', { state: { data: { ...q } } })} >
                                        <ListItemIcon>
                                            <EditIcon />
                                        </ListItemIcon>
                                    </ListItemButton>
                                    <ListItemButton>
                                        <ListItemIcon onClick={() => navigate('../bonusquestionlist', { state: { data: { ...q } } })}>
                                            {/* if bonusPresent then show in different color*/}
                                            <AutoAwesomeMotionIcon style={q.bonusPresent? { color: theme.palette.primary.light } : { color: "grey" } }/>
                                        </ListItemIcon>
                                    </ListItemButton>
                                </ListItem>
                            )}
                        </List>
                    </Box>
                </Grid>
            </Grid>


        </Fragment>


    )
}
