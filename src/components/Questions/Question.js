import DoneIcon from '@mui/icons-material/Done';
import PlaylistAddCircleIcon from '@mui/icons-material/PlaylistAddCircle';
import {
    ButtonBase, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    FormControl, Input, InputLabel, Link, List, ListItem, ListItemButton, ListItemIcon, ListItemText, MenuItem, Select
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import { createTheme, styled, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { convertFromHTML, convertToHTML } from 'draft-convert';
import { convertFromRaw, convertToRaw, EditorState } from 'draft-js';
import MUIRichTextEditor from 'mui-rte';
import React, { useCallback, useEffect, useState } from 'react';
import { getLeafTags } from '../../api/tags';
import notifications from '../../utils/notifications';
import CircularIndeterminate from '../shared/CircularIndeterminate';
import Copyright from '../shared/Copyright';
import TagsInput from '../shared/TagsInput';


const Img = styled('img')({
    margin: 'auto',
    display: 'block',
});

const theme = createTheme();

Object.assign(theme, {
    overrides: {
        MUIRichTextEditor: {
            root: {
                marginTop: 20,
                width: "100%",
                border: "0.1px solid gray",
                borderRadius: "4px"
            },
            editor: {
            }
        }
    }
})

export const Question = ({
    data,
    submitAction
}) => {

    const [imagePath, setImagePath] = useState(data && data.image ? data.image : "assets/default_image.jpg")
    const [imgFile, setImgFile] = useState(null)
    const [correctoption, setCorrectoption] = useState(data && data.correct_option ? data.correct_option : 1)
    const [tags, setTags] = useState(data && data.tags ? data.tags : [])
    const [tagsOption, setTagsOption] = useState([])
    const [submitting, setSubmitting] = useState(false)
    const [submitText, setSubmitText] = useState("Submit")
    const [submitted, setSubmitted] = useState(false)
    const [didyouknowEdited, setdidyouknowEdited] = useState(false)
    const [creditDlgOpen, setCreditDlgOpen] = useState(false)
    const [credits, setCredits] = useState(data && data.credits ? data.credits : [])
    const [crdtText, setCrdtText] = useState()
    const [crdtUrl, setCrdtUrl] = useState()
    const initialDidYouKnowState = data && data.didyouknow ?
        convertFromHTML(data.didyouknow)
        : EditorState.createEmpty().getCurrentContent()
    const [didyouknowState, setDidyouknowState] = useState(initialDidYouKnowState)

    const handleCreditDlgClose = () => {
        setCrdtText()
        setCrdtUrl()
        setCreditDlgOpen(false)
    };
    const handleCreditAdd = () => {
        credits.push({
            text: crdtText,
            url: crdtUrl
        })
        setCredits(credits)
        setCrdtText()
        setCrdtUrl()
        setCreditDlgOpen(false)
    }
    const handleCreditRemove = (crdtTxt) => {
        setCredits(credits.filter(cr => cr.text != crdtTxt))
    }
    const handleCreditDlgOpen = () => {
        setCreditDlgOpen(true)
    };

    const handleCorrectOptionChange = (event) => {
        setCorrectoption(event.target.value);
    };

    const handleRichTextChange = (contentState) => {
        setSubmitted(false)
        setdidyouknowEdited(true)

    }

    const handleRichTextSave = (rawState) => {
        setDidyouknowState(convertFromRaw(JSON.parse(rawState)))
        setdidyouknowEdited(false)
    }

    const handleFileSelect = (event) => {
        const reader = new FileReader();
        const file = event.currentTarget.files[0]
        if (file) {
            reader.readAsDataURL(file)
            reader.addEventListener("load", function () {
                // convert image file to base64 string
                setImagePath(reader.result)
            }, false);

            setImgFile(file)
        }
    }
    const handleSubmit = useCallback((event) => {
        if (event) {
            if (didyouknowEdited) {
                notifications.show({
                    title: 'Cannot Submit',
                    body: 'Please save "Did you know" before submitting.'
                });
                event.preventDefault();
            } else {
                setSubmitting(true)
                setSubmitText('Submitting')
                event.preventDefault();
                const formData = new FormData(event.currentTarget);
                const options = []
                for (let index = 0; index < 4; index++) {
                    const str = `option_${index + 1}`
                    options.push(formData.get(str))
                }
                const didYouKnowString = convertToHTML(didyouknowState)
                const questionData = {
                    yt_video_id: formData.get('yt_video_id'),
                    question: formData.get('question'),
                    hint: formData.get('hint'),
                    imgFile,
                    options,
                    correct_option: correctoption,
                    didyouknow: didYouKnowString,
                    tags,
                    credits
                };

                submitAction(questionData).then(() => {
                    console.log("Submitted")
                    setSubmitting(false)
                    setSubmitText("Submitted")
                    setSubmitted(true)
                })
            }

        }
    })
    useEffect(() => {
        handleSubmit()
    }, [handleSubmit])


    useEffect(() => {
        getLeafTags().then((tags) => setTagsOption(tags))
    }, [])


    const onTagChange = (event, newValue) => {
        console.log(newValue);
        setTags(newValue)
    }

    const onFormEdit = (event) => {
        console.log("FormEdited")
        setSubmitted(false)
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="800">
                <CssBaseline />

                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center'
                    }}
                >
                    <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
                        <PlaylistAddCircleIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Add a Question
                    </Typography>
                    <Box component="form" noValidate onChange={onFormEdit}
                        onSubmit={handleSubmit} sx={{ mt: 3 }}>
                        <Grid
                            container
                            spacing={2}
                            justifyContent="center"
                            alignItems="center">

                            <Grid item xs='auto'>
                                <Input
                                    accept="image/*"
                                    id="raised-button-file"
                                    style={{ display: 'none' }}
                                    type="file"
                                    onChange={handleFileSelect}
                                />
                                <label htmlFor="raised-button-file">
                                    <ButtonBase component="span">
                                        <Img alt="complex" src={imagePath} />
                                    </ButtonBase>
                                </label>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="yt_video_id"
                                    fullWidth
                                    multiline
                                    defaultValue={data && data.yt_video_id ? data.yt_video_id : ""}
                                    id="yt_video_id"
                                    label="Youtube Video ID"
                                    autoFocus
                                />
                            </Grid>

                            <Grid item xs={12}>
                                {/* Youtube Video ID (in the URL v=? )*/}
                                <TextField
                                    name="question"
                                    required
                                    fullWidth
                                    multiline
                                    defaultValue={data && data.question ? data.question : ""}
                                    id="question"
                                    label="Describe the Question"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    name="hint"
                                    fullWidth
                                    multiline
                                    defaultValue={data && data.hint ? data.hint : ""}
                                    id="hint"
                                    label="Hint"
                                    autoFocus
                                />
                            </Grid>
                            {[...Array(4)].map((x, i) =>
                                <Grid key={i} item xs={12}>
                                    <TextField
                                        required
                                        fullWidth
                                        defaultValue={data && data.options? data.options[i] : ""}
                                        id={`option_${i + 1}`}
                                        label={`Option ${i + 1}`}
                                        name={`option_${i + 1}`}
                                    />
                                </Grid>
                            )}
                            <Grid item xs={12}>
                                <FormControl fullWidth>
                                    <InputLabel id="correct_option">Correct Option</InputLabel>
                                    <Select
                                        labelId="correct_option"
                                        id="correct_option"
                                        label="Correct Option"
                                        value={correctoption}
                                        onChange={handleCorrectOptionChange}
                                    >
                                        <MenuItem value={0}>Option 1</MenuItem>
                                        <MenuItem value={1}>Option 2</MenuItem>
                                        <MenuItem value={2}>Option 3</MenuItem>
                                        <MenuItem value={3}>Option 4</MenuItem>
                                    </Select>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} minHeight={200}>
                                <MUIRichTextEditor
                                    label="Did you know..."
                                    onBlur={handleRichTextChange}
                                    onSave={handleRichTextSave}
                                    defaultValue={JSON.stringify(convertToRaw(didyouknowState))}


                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button onClick={handleCreditDlgOpen} >
                                    Add Credits
                                </Button>
                                <Dialog open={creditDlgOpen} onClose={handleCreditDlgClose}>
                                    <DialogTitle>Add Credits</DialogTitle>
                                    <DialogContent>
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Link Text"
                                            onChange={(e) => setCrdtText(e.target.value)}
                                            fullWidth
                                            variant="standard"
                                        />
                                        <TextField
                                            autoFocus
                                            margin="dense"
                                            id="name"
                                            label="Link URL"
                                            onChange={(e) => setCrdtUrl(e.target.value)}
                                            fullWidth
                                            variant="standard"
                                        />
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleCreditDlgClose}>Cancel</Button>
                                        <Button onClick={handleCreditAdd}>Add</Button>
                                    </DialogActions>
                                </Dialog>
                            </Grid>
                            <Grid item xs={12}>
                                <Box
                                    sx={{
                                        width: 500,
                                        marginTop: 2,
                                        marginLeft: 5,
                                        display: 'flex',
                                        flexDirection: 'row',
                                        alignItems: 'start',

                                    }}
                                >
                                    <List sx={{ width: '100%', maxWidth: 800, bgcolor: 'background.paper' }}>
                                        {credits.map((cr) =>
                                            <ListItem alignItems="flex-start" key={cr.text}>
                                                <ListItemButton href={cr.url} target="_blank" >
                                                    <ListItemText primary={cr.text} />
                                                </ListItemButton>
                                                <ListItemButton onClick={() => handleCreditRemove(cr.text)}>
                                                    <ListItemIcon>
                                                        <DeleteIcon />
                                                    </ListItemIcon>
                                                </ListItemButton>
                                            </ListItem>

                                        )}


                                    </List>

                                </Box>
                            </Grid>
                            <Grid item xs={12}>
                                <TagsInput
                                    options={tagsOption}
                                    onChange={onTagChange}
                                    inputTags={data && data.tags ? data.tags : []}
                                />
                            </Grid>

                        </Grid>

                        {!submitting ?
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                {submitted ? <DoneIcon /> : ""}
                                {submitText}
                            </Button> :
                            <CircularIndeterminate />
                        }
                    </Box>
                </Box>
                <Copyright sx={{ mt: 5 }} />
            </Container>
        </ThemeProvider>
    )
}
