import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import { editQuestion } from '../../api/question'
import { contextActions } from '../../reducers/context-slice'
import { Question } from './Question'

export const EditQuestion = () => {
    const location = useLocation()
    const navigate = useNavigate()
    const data = location.state.data
    const dispatch = useDispatch()

    const submitAction = async (questionData) => {
        questionData.bonusParent = data.bonusParent ? data.bonusParent : ""
        editQuestion(questionData, data.id).then(() => {
            console.log("Saved question")
            navigate(-1)
        }).catch(error => {
            console.log(error)
        })
    }

    useEffect(() => {
        dispatch(contextActions.contextChanged('Edit  Question'))
    }, [])


    return (
        <Question
            data={data}
            submitAction={submitAction}
        />
    )
}
