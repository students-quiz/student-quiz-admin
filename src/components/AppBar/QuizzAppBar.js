import MenuIcon from '@mui/icons-material/Menu';
import NotificationsIcon from '@mui/icons-material/Notifications';
import Badge from '@mui/material/Badge';
import IconButton from '@mui/material/IconButton';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import { useSelector } from 'react-redux';
import { StyledAppBar } from './StyledAppBar';

export function QuizzAppBar(props) {
  const title = useSelector(({context}) => context.title)
  return (<StyledAppBar position="absolute" open={props.open}>
    <Toolbar sx={{
      pr: '24px' // keep right padding when drawer closed
    }}>
      <IconButton edge="start" color="inherit" aria-label="open drawer" onClick={props.toggleDrawer} sx={{
        marginRight: '36px',
        ...(props.open && {
          display: 'none'
        })
      }}>
        <MenuIcon />
      </IconButton>
      <Typography component="h1" variant="h6" color="inherit" noWrap sx={{
        flexGrow: 1
      }}>
        {title}
      </Typography>
      <IconButton color="inherit">
        <Badge badgeContent={4} color="secondary">
          <NotificationsIcon />
        </Badge>
      </IconButton>
    </Toolbar>
  </StyledAppBar>);
}
