/* eslint-disable no-use-before-define */
import React from "react";
import Autocomplete from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";


export default function TagInput({options, onChange, onInputChange, inputTags}) {

  return (
    <div>
      <Autocomplete
        multiple
        id="tags-outlined"
        options={options}
        defaultValue={inputTags}
        freeSolo
        onChange={onChange}
        onInputChange={onInputChange}
        renderInput={params => (
          <TextField
            {...params}
            variant="outlined"
            label="Add Tags"
            placeholder="Tags"
          />
        )}
      />
    </div>
  );
}

