
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import {
  HashRouter as Router, Route, Routes
} from 'react-router-dom';
import { login } from './api/auth';
import { Dashboard } from './components/dashboard/Dashboard';
import AddQuestion from './components/Questions/AddQuestion';
import BonusQuestionsList from './components/Questions/BonusQuestionsList';
import { EditQuestion } from './components/Questions/EditQuestion';
import { Question } from './components/Questions/Question';
import QuestionsList from './components/Questions/QuestionsList';
import store from './store';
import Home from './views/Home';
import Welcome from './views/Welcome';


export default function App() {

  useEffect(async () => {
    if (process.env.FIREBASE_DEV_EMAIL && process.env.FIREBASE_DEV_PASSWORD ) {
      const loginData = {
        email: process.env.FIREBASE_DEV_EMAIL,
        password: process.env.FIREBASE_DEV_PASSWORD
      }
      await login(loginData)
    }
  }, [])


  return (
    <Provider store={store}>
      <Router>
        <div className='content-wrapper'>
          <Routes>

            <Route path="/" exact element={<Welcome />} />
            <Route path="home" element={<Home />} >
              <Route path="" element={<Dashboard />} />
              <Route path="dashboard" element={<Dashboard />} />
              <Route path="addquestion" element={<AddQuestion />} />
              <Route path="questionlist" element={<QuestionsList />} />
              <Route path="bonusquestionlist" element={<BonusQuestionsList />} />
              <Route path="question" element={<EditQuestion />} />
            </Route>
          </Routes>
        </div>

      </Router>
    </Provider>

  )
}
