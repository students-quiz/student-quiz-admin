import * as api from '../api/auth';
import { authActions } from '../reducers/auth-slices';



export const loginUser = formData => dispatch => {
  dispatch(authActions.loginInitiated())
  return api
    .login(formData)
    .then(user => {
      if (user.roles.admin) {
        dispatch(authActions.login());
      }else{
        dispatch(authActions.loginProblem())
      }

    })
    .catch(error => {
      console.log(error)
      dispatch(authActions.loginProblem() )
    })
}

export const autoLoginUser = () => dispatch => {
  dispatch(authActions.loginInitiated())
  return api
    .autoLogin()
    .then(user => {
      if (user && user.roles && user.roles.admin) {
        dispatch(authActions.login());
      }else{
        dispatch(authActions.loginProblem())
      }

    })
    .catch(error => {
      console.log(error)
      dispatch(authActions.loginProblem() )
    })
}