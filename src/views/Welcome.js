
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { autoLoginUser } from '../actions/auth-actions';
import SignIn from '../components/SignIn';

export default function Welcome() {
    const dispatch = useDispatch()
    const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);

    useEffect(async () => {
        dispatch(autoLoginUser())
    }, [])

    if (isAuthenticated) {
        return <Navigate to="home" />
    }
    return (
        <SignIn />
    )
}