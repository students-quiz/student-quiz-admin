import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import React, { useEffect } from 'react';
import {
  Outlet
} from "react-router-dom";
import { QuizzAppBar } from '../components/AppBar/QuizzAppBar';
import { QuizzDrawer } from '../components/Drawer/QuizzDrawer';
import Notification from '../utils/notifications';

const mdTheme = createTheme();




function HomeContent() {

  const [open, setOpen] = React.useState(true);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  const appBarText = 'Dashboard (Dummy)'

  useEffect(() => {
    Notification.setup();
  }, [])

  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <QuizzAppBar open={open} toggleDrawer={toggleDrawer} appBarText={appBarText}></QuizzAppBar>
        <QuizzDrawer open={open} toggleDrawer={toggleDrawer}></QuizzDrawer>
        <Outlet />

      </Box>
    </ThemeProvider>
  );
}

export default function Home() {
  return <HomeContent />;
}
