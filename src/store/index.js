
import { configureStore as cfgStore } from '@reduxjs/toolkit';
import authSlices from '../reducers/auth-slices';
import contextSlice from '../reducers/context-slice';

const store = cfgStore({
    reducer: {
        auth: authSlices,
        context: contextSlice
    }
});

export default store;