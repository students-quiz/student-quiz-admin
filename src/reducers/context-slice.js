import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    title : 'Dashboard'
}

const contextSlice  = createSlice({
    name:'context',
    initialState : initialState,
    reducers:{
        contextChanged(state, action){
            state.title = action.payload
        }
    }
})

export default contextSlice.reducer;
export const contextActions = contextSlice.actions;