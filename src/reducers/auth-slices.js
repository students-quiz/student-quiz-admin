import { createSlice } from "@reduxjs/toolkit";



const initialAuthState = {
  isAuthenticated: false,
  isWaiting: false
};

const authSlice = createSlice({
  name: 'authentication',
  initialState: initialAuthState,
  reducers: {
    login(state) {
      state.isAuthenticated = true;
      state.isWaiting = false;
    },
    loginProblem(state) {
      state.isAuthenticated = false;
      state.isWaiting = false;
    },
    logout(state) {
      state.isAuthenticated = false;
      state.isWaiting = false;
    },
    loginInitiated(state){
      state.isWaiting = true;
    }
  },

})

export const authActions = authSlice.actions;
export default authSlice.reducer;